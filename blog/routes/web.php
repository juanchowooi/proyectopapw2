<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'inicio'])->name('inicio');

//Route::get('/dashboard', [PageController::class, 'dashboard']);



Route::get('/publicar', function () {
    return view('publicar');
});

Route::get('/message', function () {
    return view('mensajehome');
});

Route::get('/landing', function () {
    return view('landing');
});

Route::get('/borrador', function () {
    return view('borrador');
});


Route::get('/busqueda', function () {
    return view('busqueda');
});

Route::get('/eliminar', function () {
    return view('eliminado');
});

Route::get('/prueba', function () {
    return view('prueba');
});
Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');

Route::post('/busqueda', [App\Http\Controllers\HomeController::class, 'busqueda'])->name('busqueda');

Route::get('/eliminado', [App\Http\Controllers\HomeController::class, 'eliminarPost'])->name('eliminado');


Route::get('/dashboard/paginacion', [App\Http\Controllers\HomeController::class, 'paginacion']);

Route::get('/post/{id}', [App\Http\Controllers\HomeController::class, 'content'])->name('content');

Route::post('/', [App\Http\Controllers\HomeController::class, 'crear'])->name('post.crear');

Route::get('/user/{id}', [App\Http\Controllers\HomeController::class, 'perfil'])->name('perfil');

Route::get('/settings', [App\Http\Controllers\HomeController::class, 'cuenta'])->name('cuenta');

Route::get('/perfil', [App\Http\Controllers\HomeController::class, 'miperfil'])->name('miperfil');

Route::put('/settings/{id}', [App\Http\Controllers\HomeController::class, 'update'])->name('user.update');

Route::get('/edit/{id}', [App\Http\Controllers\HomeController::class, 'editpost'])->name('post.editar');

Route::get('/error', [App\Http\Controllers\HomeController::class, 'editpost'])->name('post.editarerror');

Route::put('/edit/{id}', [App\Http\Controllers\HomeController::class, 'updatePost'])->name('post.update');

Route::delete('/eliminar/{id}', [App\Http\Controllers\HomeController::class, 'eliminarPost'])->name('post.eliminar');

Route::delete('/eliminadoC/{id}', [App\Http\Controllers\HomeController::class, 'eliminarCom'])->name('eliminadoCom');

Route::get('/conversacion', [App\Http\Controllers\HomeController::class, 'conversacion'])->name('message');


Route::get('/con', [App\Http\Controllers\HomeController::class, 'mesajehome'])->name('mensajehome');

Route::get('/conversacion/{id}', [App\Http\Controllers\HomeController::class, 'conversacionA'])->name('mensaje');

Route::post('/msj/{id}', [App\Http\Controllers\HomeController::class, 'sendMSJ'])->name('mensaje.send');

Route::post('/nuevoC/{id}', [App\Http\Controllers\HomeController::class, 'newMSJ'])->name('mensaje.nuevo');

Route::post('/like/{id}', [App\Http\Controllers\HomeController::class, 'like'])->name('like');

Route::post('/{id}', [App\Http\Controllers\HomeController::class, 'sendCOM'])->name('comentario.nuevo');

Route::get('/Bienvenido', [App\Http\Controllers\HomeController::class, 'Bienvenido'])->name('Bienvenido');


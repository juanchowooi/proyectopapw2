@extends('layouts.landingbase')

@section('title', 'LANDINGPAGE')

@section('landing')

<?php 
$bg = array('img/perfil/6.png', 
            'img/perfil/2.png',
            'img/perfil/3.png');
$i = rand(0, count($bg)-1);
$img = "";
foreach($posts as $po){
    $img = base64_encode($po->imagen1);
}
//$selectedBg = "data:image/png;base64,.$img.";
$selectedBg = $bg[$i];
?>
<!--{{"data:image/png;base64,.$img."}}-->
<style type="text/css">
    .page1{padding-top: 40px;padding-bottom: 40px;
    background: url('<?php echo $selectedBg; ?>') no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;}
</style>


    <div class="control">
        
        <div class="tooltip"><button class="btn" onclick="location.href = '#section1';"></button>
         <span class="tooltiptext">Log In</span>
        </div>
        <div class="tooltip"><button class="btn" onclick="location.href = '#section2';"></button>
         <span class="tooltiptext">Registrate</span>
        </div>
        <div class="tooltip"><button class="btn" onclick="location.href = '#section3';"></button>
         <span class="tooltiptext">About us</span>
        </div>
    </div>
    <div class="showcase">
        <div class="page1" id="section1">
        <!--<img class="background" src="data:image/png;base64, {{ base64_encode($bg[$i]) }}" onerror="this.onerror=null; this.src='img/default.png'">-->
            <div class="flex">
                <div class ="text" >
                    <label class ="titulo">Yumbler</label>
                    <hr class="linea">
                </div>
                <div class = "login"  id="myform">
                    <label class ="subtitulo1">Entra y comparte tu arte</label>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <div class ="col-25">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                </div>
                                <div class="col-75">
                                    <input id="email" type="email" class="texto" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class ="col-25">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                </div>
                                <div class="col-75">
                                    <input id="password" type="password" class="texto" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                    <div class="col-25">
                                    <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                        </div>
                                        <div class="col-25">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        </div>
                            </div>

                            <div class="form-group row">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                    <div class="col-75">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}" style= "margin-left: auto; margin-right: auto;" >
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                    </div>
                            </div>
                        </form>

                </div>
            </div>
        </div>
        <div class="page2" id="section2">
            <div class="flex">
                <div class ="text" >
                    <label class ="subtitulo">Unete a la comunidad y comparte tu arte junto con otros artistas</label>
                </div>
                <div class = "login"  id="myform">
                <label class ="subtitulo2">Registrate</label>
                    <form method="POST" action="{{ route('register') }}" class = "form">
                        @csrf
                        <div class="row">
                            <div class="form-group row">
                                    <div class ="col-25">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                    </div>
                                        <div class="col-75">
                                            <input id="name" type="text" class="texto" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                            </div>

                            <div class="form-group row">
                            <div class ="col-25">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                </div>
                                <div class="col-75">
                                    <input id="email" type="email" class="texto" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                            <div class="col-25">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                </div>
                                <div class="col-75">
                                    <input id="password" type="password" class="texto" name="password" required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-25">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                                </div>
                                <div class="col-75">
                                    <input id="password-confirm" type="password" class="texto" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-25">
                                </div>
                                <div class="col-75" >
                                    <button type="submit" class="btn btn-primary">
                                            {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="page3" id="section3">
            <div  class = "info">
                <label class ="subtitulo">Yumbler es la pagina donde puedes compartir tu arte y resibir retro alimentacion junto con otros artistas </label>
            </div>
        </div>
    </div>

@endsection 
@extends('layouts.base')

@section('title', 'Pagina | Cuenta')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection


@section('cuenta')
<div class="container">
    <div class="main">
    <article>
            <div class="wraperr">
                <section class="postt">

                    <form action="{{ route('user.update', Auth::user()->id) }}" name="publicar" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                            <div class="caption1">
                                <div class = "row">
                                    <div class="col-25">
                                    <label></label>
                                    </div>
                                    <div class="col-75">
                                    </div>
                                    <div class="col-5">
                                        <input type="file" id="myFile" name="imagen1" onchange = "readURL1(this);" class="inputfile">
                                        <label for="myFile">editar</label>
                                    </div>
                                    <img id="imaasdages1" class="cuentaAvatar" src="data:image/png;base64, {{ base64_encode(Auth::user()->avatar) }}">
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Usuario:</label>
                                    </div>
                                    <div class="col-75">
                                    <input name="nombre" value="{{Auth::user()->name}}"></>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Correo:</label>
                                    </div>
                                    <div class="col-75">
                                        <label>{{Auth::user()->email}}</label>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Contraseña:</label>
                                    </div>
                                    <div class="col-75">
                                    <label>*********</label>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Titulo:</label>
                                    </div>
                                    <div class="col-75">
                                        <input name="titulo" value="{{Auth::user()->titulo}}"></>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Descripcion:</label>
                                    </div>
                                    <div class="col-75">
                                        <textarea name="descr" cols="45" rows="5">{{Auth::user()->descripcion}}</textarea>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Banner:</label>
                                    </div>
                                    <div class="col-75">
                                    </div>
                                    <div class="col-5">
                                        <input type="file" id="myFile2" name="imagen2" onchange = "readURL2(this);" >
                                        <label for="myFile2">editar</label>
                                    </div>
                                    <img class="baner" id="images2" src="data:image/png;base64, {{ base64_encode(Auth::user()->banner) }}">
                                    <input type="submit" value="UPDATE" name="submitbutton"> 
                                </div>
                                <div class = "linea"></div>
                                @if (session('mensaje'))

                                <div class="alert alert-success"> {{session('mensaje')}}</div>
                                        
                                    @endif
                            </div>    
                        </form>                   
                </section>
            </div>
        </article>
        </div>
</div>

<script>
        function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imaasdages1')
                .attr('src', e.target.result)
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#images2')
                .attr('src', e.target.result)
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection
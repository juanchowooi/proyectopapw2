<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/landing.css')}}">
    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>   
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" ></script>
    <title>@yield('title')</title>
</head>
<body>
    @yield('landing')
</body>
</html>
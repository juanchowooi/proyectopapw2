<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('meta')
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/like.css')}}">
    <link rel="stylesheet" href="{{asset('css/post.css')}}">
    <link rel="stylesheet" href="{{asset('css/cuenta.css')}}">
    <link rel="stylesheet" href="{{asset('css/prueba.css')}}">
    <link rel="stylesheet" href="{{asset('css/publicar.css')}}">
    @yield('P-css')
    @yield('M-css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>@yield('title')</title>

    <script src="https://kit.fontawesome.com/44b1b8aa51.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.jscroll.min.js"></script>
    <script type="text/javascript" src="js/varmenu.js"></script>
    <script type="text/javascript" src="js/like.js"></script>
    <script type="text/javascript" src="js/imagen.js"></script>
    <script type="text/javascript" src="js/loadIMG.js"></script>

</head>
<body>

    @yield('banner')

    <img class="postear" src="/img/plus.jpg" onclick = "location.href = 'http://127.0.0.1:8000/publicar';"> 

    <div class="head">

    <header>
        <a href="http://127.0.0.1:8000/" class="logo"></a>
        <ul>    
            <li><a class="home" href="http://127.0.0.1:8000/dashboard"><i class="fas fa-home"></i></a></li>
        </ul>

        <form action="{{ route('busqueda') }}" name="buscar" method="POST" enctype="value">
        @csrf
            @error('nombre')
            <script>
                alert("I am an alert box!");
                </script>
            @enderror
        <div class="search-box">
            <input class="search-txt" type="text" name="buscador" placeholder="Search" id="busque">
            <button class="search-btn" type="submit" value="publicar" name="submitbutton">
                <i class="fas fa-search"></i>
            </button>
        </div>
        </form>
        <div class="icon">
            <div class="i-img" onclick="myFunction()">
                <img class="i-imgg" src="data:image/png;base64, {{ base64_encode(Auth::user()->avatar) }}" onerror="this.onerror=null; this.src='img/default.png'">
            </div>
            <div class="i-arrow">
                <i class="fas fa-arrow-down"></i>
            </div>
            <div id="dropmenu" style="display: none">
                <div class="i-flex">
                    <div class="ii-img" onclick="window.location='{{ route('perfil', Auth::user()->id) }}'">
                        <img class="ii-imgg" src="data:image/png;base64, {{ base64_encode(Auth::user()->avatar) }}" onerror="this.onerror=null; this.src='img/default.png'">
                    </div>
                    <div class="i-user">
                        <p class="i-usertxt">{{ Auth::user()->name }}</p>
                        <p class="i-arrtxt">{{ Auth::user()->email }}</p>
                    </div>
                </div>

                <hr class="hrr">
                <div class="i-link">
                        <li class="i-li"><a class="i-conf" href="{{ route('perfil', Auth::user()->id) }}">Perfil</a></li>
                        <li class="i-li"><a class="i-conf" href="http://127.0.0.1:8000/con">Mensajes</a></li>
                        <li class="i-li"><a class="i-conf" href="{{ route('cuenta') }}">Configuracion</a></li>
                </div>
                <div>      
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                        <input type="submit" value = "Salir">
                    </form>
                    
                </div>
            </div>
        </div>
    </header>
    </div>
     
    @yield('container')

    @yield('container2')
    
    @yield('perfil')

    @yield('cuenta')

    @yield('publicar')  

    @yield('contenido')
    
    @yield('message1')

    @yield('message')

    <footer class="footerino">

      </footer>

      <script>
        function myFunction() {
          var x = document.getElementById("dropmenu");
          if (x.style.display === "none") {
            x.style.display = "block";
          } else {
            x.style.display = "none";
          }
        }
        </script>
        

</body>
</html>
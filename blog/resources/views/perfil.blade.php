@extends('layouts.base')

@section('title', 'PERFIL')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection

@section('P-css')
    <link rel="stylesheet" href="{{asset('css/banner.css')}}">
@endsection

@section('banner')

    <div class="banner">
        <div class="img-wraper">
            <img class="img-banner" src="data:image/png;base64, {{ base64_encode($user->banner) }}" onerror="this.onerror=null; this.src='img/default.png'">
        </div>
        <div class="blog=titulo">
            <figure class="p-figure">
                <img class="user-avatar" src="data:image/png;base64, {{ base64_encode($user->avatar) }}">
            </figure>
        </div>
        <div class="tittle">
            <h1 class="p-titulo">{{ $user->titulo }}</h1>
        </div>
        <div class="descrippcion">
            <h3 class="p-descripcion">{{ $user->descripcion }}</h3>
        </div>
    </div>
@endsection

@section('perfil')

<div class="p-conn">
<div class="p-container">
    <div class="p-main">
        <ul class="p-ul">
            @foreach ($Userposts as $Useritem)
            @if ($user->email == $Useritem->usuario)
                <li class="p-li">
                <a href="{{ route('content', $Useritem) }}">
                    <div class="p-div">
                     <img class="p-img" src="data:image/png;base64, {{ base64_encode($Useritem->imagen1) }}">
                    </div>
                </a>
                    <div class="p-posttitulo">
                    <h2>{{$Useritem->nombre}}</h2>
                 </div>
                </li>
            @endif
            @endforeach
        </ul>
    </div>
</div>
<div  class="p-friends">
    <h1 class="txtFriends">FRIENDS</h1>
    <ul class="p-ulF">
                @foreach ($conversacion as $connv) 
                @if ($connv->usuario1 == $user->id or $connv->usuario2 == $user->id)
                @foreach ($userB as $usera)
                @if ($usera->id == $connv->usuario1 and $connv->usuario1 != $user->id)
                <a href="{{ route('perfil', $usera->id) }}">
                <li class="p-liF">
                    <div class="p-friend">
                        <img class="p-imgF" src="data:image/png;base64, {{ base64_encode($usera->avatar) }}">
                    </div>
                </li>
                 </a>
                @endif
                @if ($usera->id == $connv->usuario2 and $connv->usuario2 != $user->id)
                <a href="{{ route('perfil', $usera->id) }}">
                <li class="p-liF">
                    <div class="p-friend">
                        <img class="p-imgF" src="data:image/png;base64, {{ base64_encode($usera->avatar) }}">
                    </div>
                </li>
                 </a>
                @endif  
                @endforeach
                @endif
                @endforeach
    </ul>
</div>
<div class="FbtnMbtn">

        <form action="{{ route('mensaje.nuevo', $user->id) }}" name="publicar" method="POST" enctype="multipart/form-data">

            @csrf 
        <button class="btnFollow" type="submite"  href="http://127.0.0.1:8000/conversacion">{{ $text }}</button>
        </form> 
         

   
</div>
</div>
    
@endsection
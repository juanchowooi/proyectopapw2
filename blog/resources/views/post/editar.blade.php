@extends('layouts.base')

@section('title', 'Pagina | Cuenta')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection


@section('publicar')

@if (session('mensaje'))
    <div class="alert alert-success">
        {{ session('mensaje') }}
    </div>
@endif

<div class="container3">

    <div class="slideshow-container">

    <div class="mySlides fade">
      <div class="numbertext">1 / 3</div>
      <img id="images1" src="data:image/png;base64, {{ base64_encode($post->imagen1) }}" style="width:100%">
    </div>
    
    <div class="mySlides fade">
      <div class="numbertext">2 / 3</div>
      <img id="images2" src="data:image/png;base64, {{ base64_encode($post->imagen2) }}" style="width:100%">
    </div>
    
    <div class="mySlides fade">
      <div class="numbertext">3 / 3</div>
      <img id="images3" src="data:image/png;base64, {{ base64_encode($post->imagen3) }}" style="width:100%">
    </div>
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>
    
    </div>
    
    <div class="containerPOST">
        <form action="{{ route('post.update', $post->id) }}" name="publicar" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            @error('nombre')
            <script>
                alert("I am an alert box!");
                </script>
            @enderror

        <div class="containerPOST2">
        <div class="concon">
            <label>Archivo:</label>
            <input type="file" id="myFile" name="imagen1" onchange = "readURL1(this);">
        </div>
        <div class="concon">
            <label>Archivo:</label>
            <input type="file" id="myFile" name="imagen2" onchange = "readURL2(this);">
        </div>
        <div class="concon">
            <label>Archivo:</label>
            <input type="file" id="myFile" name="imagen3" onchange = "readURL3(this);">
        </div>
        <div class="concon">
            <label>Titulo:</label>
            <input type="text" id="fname" name="nombre" placeholder="Your name.." value="{{ $post->nombre }}">
        </div>
        <div class="concon">
            <label>Descipcion:</label>
            <textarea id="subject" class="desc" name="descripcion" placeholder="Write something..">{{ $post->descripcion }}</textarea>
        </div>
        <div class="concon">
            <label>Tag:</label>
            <br>
            <input class ="p-check" type="checkbox" id="filtro1" name="filro1" value="personaje">
            <label class ="p-tag" for="filtro1">Personaje</label><br>
            <input class ="p-check" type="checkbox" id="filtro2" name="filro2" value="Escena">
            <label class ="p-tag" for="filtro2"> Escena</label><br>
            <input class ="p-check" type="checkbox" id="filtro3" name="filro3" value="Dibujo">
            <label class ="p-tag" for="filtro1">Dibujo</label><br>
            <input class ="p-check" type="checkbox" id="filtro4" name="filro4" value="Foto">
            <label class ="p-tag" for="filtro2"> Foto</label><br>
            <input class ="p-check" type="checkbox" id="filtro5" name="filro5" value="3D">
            <label class ="p-tag" for="filtro1">3D</label><br>
        </div>
        <div class="concon">
            <input type="submit" value="publicar" name="submitbutton"> 
            <input type="submit" value="borrador" name="submitbutton">  
        </div>
    </div>
</form>
<form action="{{route('post.eliminar', $post)}}" method="POST">
    @method('DELETE')
    @csrf
<input type="submit" value="borrar" name="submitbutton" style="background-color: red">  
</form>
    </div>

</div>

<script>
    function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#images1')
                .attr('src', e.target.result)
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#images2')
                .attr('src', e.target.result)
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#images3')
                .attr('src', e.target.result)
        };

        reader.readAsDataURL(input.files[0]);
    }
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}

</script>
@endsection
@extends('layouts.base')

@section('title', 'PERFIL')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection

@section('P-css')
    <link rel="stylesheet" href="{{asset('css/content.css')}}">
    <link rel="stylesheet" href="{{asset('css/vote.css')}}">
@endsection

@section('contenido')
    <div class="c-container">
        <div class="c-post">
            <div class="slideshow-container">

                <!-- Full-width images with number and caption text -->
                <div class="mySlides fade">
                  <div class="numbertext">1 / 3</div>
                  <img src="data:{{$post->mime}};base64, {{ base64_encode($post->imagen1) }}" style="width:100%">
                </div>
              
                <div class="mySlides fade">
                  <div class="numbertext">2 / 3</div>
                  <img src="data:{{$post->mime}};base64, {{ base64_encode($post->imagen2) }}" style="width:100%">
                </div>
              
                <div class="mySlides fade">
                  <div class="numbertext">3 / 3</div>
                  <img src="data:{{$post->mime}};base64, {{ base64_encode($post->imagen3) }}" style="width:100%">
                </div>
              
                <!-- Next and previous buttons -->
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>
              </div>
              <br>
              
              <!-- The dots/circles -->
              <div style="text-align:center">
                <span class="dot" onclick="currentSlide(1)"></span>
                <span class="dot" onclick="currentSlide(2)"></span>
                <span class="dot" onclick="currentSlide(3)"></span>
              </div>

            <div class="c-titulo">
                {{ $post->nombre }}
            </div>
            <div class="c-sub">
                {{ $post->descripcion }}
            </div>
            <div class="c-leavecomments">
                <a href="{{ route('perfil', Auth::user()->id) }}">
                <img class="c-avatar" src="data:image/png;base64, {{ base64_encode(Auth::user()->avatar) }}">
                </a>
                <form action="{{ route('comentario.nuevo', $post->id) }}" name="publicar" method="POST" enctype="multipart/form-data">            
                    @csrf
                    <input class="c-cometario" name="comm" type="text">
                    <input class="c-btncmnt" type="submit" value="ENVIAR" name="submitbutton">  
                </form>
            
            </div>
            <div class="c-comments">
                <hr>

                @foreach ($commnet as $item)
                    @foreach ($usuarioP as $item2)
                    @if ($item2->id == $item->in_usuario)
                    <div class="c-comment">
                        <img class="c-avatar" src="data:image/png;base64, {{ base64_encode($item2->avatar) }}">
                        <div class="c-datos">
                            <h2 class="c-miusuario">{{$item2->name}}</h2>
                            <h3 class="c-micomentario">{{ $item->comentario }}</h3>
                            <h4 class="c-date">{{ $item->created_at }}</h4>
                        </div>
                        @if ($item->in_usuario == Auth::user()->id)
                        <form action="{{ route('eliminadoCom', $item->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                        <input type="submit" value="X" name="submitbutton" class="x">  
                        </form>
                        @endif    
                    </div>
                    @endif
                    @endforeach
                @endforeach

                <hr>
            </div>
        </div>
        @foreach ($usuarioP as $UserP)
        @if ($post->usuario == $UserP->email)
        <div class="c-info">
            <div class="c-user">
                <a href="{{ route('perfil', $UserP) }}">
                <img class="c-avatarI" src="data:image/png;base64, {{ base64_encode($UserP->avatar) }}" onerror="this.onerror=null; this.src='img/default.png'">
                </a>
            <h2 class="c-usuariotxt"> {{ $UserP->name }}</h2>
            <br>
            <br>
            <br>
            <form action="{{ route('like', $post->id) }}" name="publicar" method="POST" enctype="multipart/form-data">
            <label class="c"> {{$cantidad}} </label>

            @csrf 
            <button class="c-btncmnt" type="submite"  href="http://127.0.0.1:8000/conversacion">Like</button>
            </form> 
            </form> 
            </div>
        
        </div>
        @endif
        @endforeach
    </div>

    <div class="c-otros">
        <ul class="p-ulO">
            @foreach ($usuarioP2 as $Useritem)
            <li class="p-liO">
                <a href="{{ route('content', $Useritem) }}">
                <div class="muestra">
                    <img class="c-otrosIMG" src="data:image/png;base64, {{ base64_encode($Useritem->imagen1) }}">
                </div>
                </a>
            </li>
            @endforeach
        </ul>
    </div>

    <script>
        var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
    </script>
@endsection
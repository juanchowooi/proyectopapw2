@extends('layouts.base')

@section('title', 'message')

@section('M-css')
    <link rel="stylesheet" href="{{asset('css/message.css')}}">
@endsection

@section('message1')
    
    <div class="m=container">
        <div class="m-friends">
            <div class="m-friend">
                <h1 class="m-toptxt">MESSAGE</h1>
                <hr>
                <div class="m-friendcont">
                    <img class="m-avatar" src="img/avatar/avatar3.png">
                    <div class="m-datos">
                        <h2 class="m-miusuario">USUARIO1</h2>
                        <h4 class="m-date">2/16</h4>
                    </div>
                </div>
                <div class="m-friendcont">
                    <img class="m-avatar" src="img/avatar/avatar4.png">
                    <div class="m-datos">
                        <h2 class="m-miusuario">USUARIO2</h2>
                        <h4 class="m-date">4/26</h4>
                    </div>
                </div>
                <div class="m-friendcont">
                    <img class="m-avatar" src="img/avatar/avatar5.jpg">
                    <div class="m-datos">
                        <h2 class="m-miusuario">USUARIO4</h2>
                        <h4 class="m-date">12/18</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-messageclass">
            <div class="m-messagebox">
                <h3 class="m-topusertxt">USUARIO2</h3>
                <hr>
                <div class="m-messages">
                    <div class="m-messageF">
                        <img class="m-avt" src="img/avatar/avatar5.jpg">
                        <div class="m-boxF">
                            <div class="m-tetex">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book
                            </div>
                        </div>
                    </div>
                    <div class="m-messageM">
                        <div class="m-boxM">
                            <div class="m-tetex2">
                                It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </div>
                        </div>
                        <img class="m-avtM" src="img/avatar/avatar4.png">
                    </div>
                    <div class="m-messageM">
                        <div class="m-boxMS">
                            <img class="sticker" src="img/paimon.jpg">
                        </div>
                        <img class="m-avtM" src="img/avatar/avatar4.png">
                    </div>
                    <div class="m-messageF">
                        <img class="m-avt" src="img/avatar/avatar5.jpg">
                        <div class="m-boxF">
                            <div class="m-tetex">
                                XD
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="m-messagetxtbox">
                    <div class="m-boxMSJ">
                        <button class="m-btn"><i class="fas fa-images"></i></button>
                        <input class="m-input" type="text">
                        <button class="m-btnS"><i class="fas fa-paper-plane"></i> SEND</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@extends('layouts.base')

@section('title', 'message')

@section('M-css')
    <link rel="stylesheet" href="{{asset('css/message.css')}}">
@endsection

@section('message')
    
    <div class="m=container">
        <div class="m-friends">       
            <div class="m-friend">
                <h1 class="m-toptxt">MESSAGE</h1>
                <hr>
                @foreach ($conversacion as $connv) 
                @if ($connv->usuario1 == Auth::user()->id or $connv->usuario2 == Auth::user()->id)
                @foreach ($userA as $usera) 
                @if ($usera->id == $connv->usuario2 and $connv->usuario2 != Auth::user()->id)
                <a href="{{ route('mensaje', $connv->id) }}">
                    <div class="m-friendcont">
                        <img class="m-avatar" src="data:image/png;base64, {{ base64_encode($usera->avatar) }}">
                        <div class="m-datos">
                            <h2 class="m-miusuario">{{$usera->name}}</h2>
                            <h4 class="m-date">{{$usera->created_at}}</h4>
                        </div>
                    </div>  
                 </a>
                @endif
                @if ($usera->id == $connv->usuario1 and $connv->usuario1 != Auth::user()->id)
                <a href="{{ route('mensaje', $connv->id) }}">
                    <div class="m-friendcont">
                        <img class="m-avatar" src="data:image/png;base64, {{ base64_encode($usera->avatar) }}">
                        <div class="m-datos">
                            <h2 class="m-miusuario">{{$usera->name}}</h2>
                            <h4 class="m-date">{{$usera->updated_at}}</h4>
                        </div>
                    </div>  
                 </a>
                @endif
                @endforeach
                @endif
                @endforeach
            </div>
        </div>
        <div class="m-messageclass">
            <div class="m-messagebox">
                <h3 class="m-topusertxt">CHAT</h3>
                <hr>
                <div class="m-messages">
                    
                </div>
                
                <div class="m-messagetxtbox">
                    <div class="m-boxMSJ">
                        <button class="m-btn"><i class="fas fa-images"></i></button>
                        <input class="m-input" type="text">
                        <button class="m-btnS"><i class="fas fa-paper-plane"></i> SEND</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
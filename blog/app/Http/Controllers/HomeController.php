<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function dashboard()
    {
        $posts = App\Models\Post::where('status', '=', 'publicado')
        ->orderby('created_at', 'desc')
        ->paginate(3);
        $usuario = App\Models\User::all();
        return view('dashboard', compact('posts', 'usuario'));
    }
    public function busqueda(Request $request)
    {
        $posts = App\Models\Post::where('descripcion', 'LIKE' ,'%'.$request->buscador.'%')
        ->where('status', '=', 'publicado')
        ->orderby('created_at', 'desc')
        ->paginate(3);
        $usuario = App\Models\User::all();
        return view('dashboard', compact('posts', 'usuario'));
    }
    public function paginacion()
    {
        $posts = App\Models\Post::where('status', '=', 'publicado')
        ->orderby('created_at', 'desc')
        ->paginate(3);
        $usuario = App\Models\User::all();
        return view('post.pagina', compact('posts', 'usuario'));
    }
    public function content($id)
    {   
        $like = App\Models\likes::all();
        $user1 = Auth::user();
        $x = 0;
        $cantidad = 0;
        $post = App\Models\Post::findOrFail($id);
        foreach ($like as $lik) {
            if($lik->id_post == $post->id){
                    $cantidad = 1 + $cantidad;
            }
        }
        $usuarioP = App\Models\User::all();
        $usuarioP2 = App\Models\Post::all(); 
        $commnet = App\Models\comentario::where('id_post', $post->id)
        ->orderby('created_at', 'desc')
        ->get();

        return view('content', compact('post', 'usuarioP', 'usuarioP2', 'commnet','cantidad'));
    }
    public function miperfil()
    {
        $Userposts = App\Models\Post::all();
        return view('miperfil', compact('Userposts'));
    }
    public function perfil($id)
    {
        $conversacion = App\Models\conversacion::all();
        $user1 = Auth::user();
        $user2 = App\Models\User::findOrFail($id);
        $userA = App\Models\User::all();
        $text = "SEGUIR";
        $x = 0;
        foreach ($conversacion as $connv) {
            if ($connv->usuario1 == Auth::user()->id or $connv->usuario2 == Auth::user()->id){
                if ($connv->usuario1 ==  $user2 ->id or $connv->usuario2 ==  $user2 ->id){
                    $x = 1;
                }
            }
        }
        if($x == 1){
            $text = "MENSAJE";
        }
        $conversacion = App\Models\conversacion::all();
        $user = App\Models\User::findOrFail($id);
        $Userposts = App\Models\Post::all();
        $userA = Auth::user();
        $userB = App\Models\User::all();

        if($userA->id == $user->id) {
            $Userposts = App\Models\Post::all();
            return view('miperfil', compact('Userposts','conversacion','userB')); 
        } else {
            $Userposts = App\Models\Post::where('status', '=', 'publicado')
            ->orderby('created_at', 'desc')
            ->paginate(null);
            return view('perfil', compact('user', 'Userposts','conversacion','userB','text'));
        }
    }
    public function cuenta()
    {
        return view('cuenta');
    }
    public function crear(Request $request)
    {
        
        //return $request->all();

        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
            'imagen1' => 'required',
            'imagen2' => 'required',
            'imagen3' => 'required'
        ]);

        switch($request->submitbutton) {

            case 'publicar':

                $postNuevo = new App\Models\Post;
                $postNuevo->nombre = $request->nombre;
                $postNuevo->descripcion = $request->descripcion; 
                $postNuevo->status = "publicado";
                $postNuevo->usuario = auth()->user()->email;
                $postNuevo->imagen1 = file_get_contents($request->imagen1);
                $postNuevo->imagen2 = file_get_contents($request->imagen2); 
                $postNuevo->imagen3 = file_get_contents($request->imagen3); 
                $postNuevo->save();

        return back()->with('mensaje', 'Post Agregado!');
            break;

            case 'borrador':
                $postNuevo = new App\Models\Post;
                $postNuevo->nombre = $request->nombre;
                $postNuevo->descripcion = $request->descripcion; 
                $postNuevo->status = "borrador";
                $postNuevo->usuario = auth()->user()->email;
                $postNuevo->imagen1 = file_get_contents($request->imagen1);
                $postNuevo->imagen2 = file_get_contents($request->imagen2); 
                $postNuevo->imagen3 = file_get_contents($request->imagen3); 
                $postNuevo->save();

                return back()->with('mensaje', 'Post Agregado!');
            break;

        }
    }
    public function update(Request $request, $id)
    {
        $userUpdate = App\Models\User::findOrFail($id);
        if ($request->imagen1 != "") {
            $userUpdate->avatar = file_get_contents($request->imagen1);
        }
        $userUpdate->name = $request->nombre;
        $userUpdate->titulo = $request->titulo;
        $userUpdate->descripcion = $request->descr;
        if ($request->imagen2 != "") {
            $userUpdate->banner = file_get_contents($request->imagen2);
        }
        $userUpdate->save();

        return back()->with('mensaje', 'usuario actualizado');  
    }
    public function editpost($id)
    {
        $post = App\Models\Post::findOrFail($id);
        $userA = Auth::user();
        if($userA->email == $post->usuario) {
            return view('post.editar', compact('post'));   
        } else {
            return view('post.editarerror'); 
        }
    }
    public function updatePost(Request $request, $id)
    {
        switch($request->submitbutton) {

            case 'publicar':
                    $postUpdate = App\Models\Post::findOrFail($id);
                    if ($request->nombre != "") {
                    $postUpdate->nombre = $request->nombre;
                    }
                    if ($request->descripcion != "") {
                    $postUpdate->descripcion = $request->descripcion; 
                    }
                    $postUpdate->status = "publicado";
                    $postUpdate->usuario = auth()->user()->email;
                    if ($request->imagen1 != "") {
                    $postUpdate->imagen1 = file_get_contents($request->imagen1);
                    }
                    if ($request->imagen2 != "") {
                    $postUpdate->imagen2 = file_get_contents($request->imagen2); 
                    }
                    if ($request->imagen3 != "") {
                    $postUpdate->imagen3 = file_get_contents($request->imagen3); 
                    }
                    $postUpdate->save();
                    return back()->with('mensaje', 'Post Actualizado!');
                break;

            case 'borrador':
                $postUpdate = App\Models\Post::findOrFail($id);
                if ($request->nombre != "") {
                $postUpdate->nombre = $request->nombre;
                }
                if ($request->descripcion != "") {
                $postUpdate->descripcion = $request->descripcion; 
                }
                $postUpdate->status = "borrador";
                $postUpdate->usuario = auth()->user()->email;
                if ($request->imagen1 != "") {
                $postUpdate->imagen1 = file_get_contents($request->imagen1);
                }
                if ($request->imagen2 != "") {
                $postUpdate->imagen2 = file_get_contents($request->imagen2); 
                }
                if ($request->imagen3 != "") {
                $postUpdate->imagen3 = file_get_contents($request->imagen3); 
                }
                $postUpdate->save();
                return back()->with('mensaje', 'Post Actualizado!');
            break;
        }
    }
    public function eliminarPost($id)
    {
        $postDelete = App\Models\Post::findOrFail($id);
        $postDelete->delete();
        $userA = Auth::user();

        return view('eliminado'); 
    }
    public function eliminarCom($id)
    {
        $ComentarioDelete = App\Models\comentario::findOrFail($id);
        $ComentarioDelete->delete();
        $post = App\Models\Post::findOrFail($id);

        return view('eliminadoCom', compact('post'));
    }
    public function conversacion()
    {
        $user = Auth::user();
        $conversacion = App\Models\conversacion::all();
        $userA = App\Models\User::all();

        return view('message', compact('user', 'conversacion', 'userA'));

    }
    public function conversacionA($id)
    {
        $conversacionA = App\Models\conversacion::findOrFail($id);
        $user = Auth::user();
        $conversacion = App\Models\conversacion::all();
        $userA = App\Models\User::all();
        $msj = App\Models\mensaje::all();

        return view('mensaje', compact('user', 'conversacionA', 'userA', 'conversacion', 'msj'));

    }
    public function sendMSJ(Request $request, $id)
    {
        $conversacionA = App\Models\conversacion::findOrFail($id);
        $msjNuevo = new App\Models\mensaje;
        $msjNuevo->mensaje = $request->mensaje;
        $msjNuevo->id_conversacion = $conversacionA->id;
        $msjNuevo->id_usuario = auth()->user()->id;
        $msjNuevo->save();

        return back()->with('mensaje', 'mensaje enviado!');
    }
    public function newMSJ(Request $request, $id)
    {
        $conversacion = App\Models\conversacion::all();
        $user1 = Auth::user();
        $user2 = App\Models\User::findOrFail($id);
        $userA = App\Models\User::all();
        $x = 0;
        foreach ($conversacion as $connv) {
            if ($connv->usuario1 == Auth::user()->id or $connv->usuario2 == Auth::user()->id){
                if ($connv->usuario1 ==  $user2 ->id or $connv->usuario2 ==  $user2 ->id){
                    $x = 1;
                }
            }
        }
        if($x == 1){
            return view('message', compact('user1', 'conversacion', 'userA'));
        }
        else{
        $conversacion = App\Models\conversacion::all();
        $conversacionNuevo = new App\Models\conversacion;
        $conversacionNuevo->usuario1 = auth()->user()->id;
        $conversacionNuevo->usuario2 = $user2->id;
        $conversacionNuevo->save();
        return back()->with('mensaje', 'mensaje enviado!');
        }

    }
    public function like(Request $request, $id)
    {
        $like = App\Models\likes::all();
        $user1 = Auth::user();
        $post = App\Models\Post::findOrFail($id);
        $postA = App\Models\Post::all();
        $x = 0;
        $cantidad = 0;
        $idlike =0;
        foreach ($like as $lik) {
            if($lik->id_post == $post->id){
                if ($lik->in_usuario ==  $user1->id){
                    $x = 1;
                    $cantidad = 1 + $cantidad;
                    $idlike = $lik->id;
                }
            }
        }
        if($x == 1){
            $likedelete = App\Models\likes::findOrFail($idlike);
            $likedelete->delete();
            return back()->with('mensaje', 'mensaje enviado!');
        }
        else{
            $like = App\Models\likes::where('id_post', '=', $id);
            $likenuevo = new App\Models\likes;
            $likenuevo->in_usuario = $user1->id;
            $likenuevo->id_post = $post->id;
            $likenuevo->save();
        return back()->with('mensaje', 'mensaje enviado!');
        }

    }
    public function sendCOM(Request $request, $id)
    {
        $POSTA = App\Models\Post::findOrFail($id);
        $commNuevo = new App\Models\comentario;
        $commNuevo->comentario = $request->comm;
        $commNuevo->id_post = $POSTA->id;
        $commNuevo->in_usuario = auth()->user()->id;
        $commNuevo->save();

        return back()->with('mensaje', 'mensaje enviado!');
    }
    public function avatar(Request $avatar)
    {
        return $request->all();
    }
    public function mesajehome()
    {
        $user = Auth::user();
        $conversacion = App\Models\conversacion::all();
        $userA = App\Models\User::all();
        
        return view('message', compact('user', 'conversacion', 'userA'));

    }
    public function Bienvenido()
    {
        $Post = App\Models\conversacion::all();
        
        return view('landing', compact('Post'));

    }
}
